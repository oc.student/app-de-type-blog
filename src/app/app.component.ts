import { Component, ɵCompiler_compileModuleAndAllComponentsAsync__POST_R3__ } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Mon application de type blog';

  /*
   * Il n'est pas logique de d'initialiser la liste des posts dans ce composant, mais c'est ce qui est demandé
   * (sûrement pour illustrer le passage de variables d'un composant à l'autre)
   * Personnellement, j'aurais initialisé la variable "posts"  dans post-list.component.ts
   * 
  */
  posts = [
    new Post(
      "Mon premier post",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut "
        + "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco "
        + "laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in ..."
    ),
    new Post(
      "Mon deuxième post",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut "
        + "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco "
        + "laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in "
        + "voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat ..."
    ),
    new Post(
      "Encore un post",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut "
        + "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco "
        + "laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in "
        + "voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat "
        + "non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    ),
  ]

}
